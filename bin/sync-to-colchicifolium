#!/bin/sh
#
# Script used to sync the descriptor tarball, bridge assignments, and BridgeDB's
# metrics to a Metrics server.
#
# It is called from the store-bridge-tarball script.

set -e
set -u

SYNC_DESCRIPTORS=true
SYNC_ASSIGNMENTS=true
SYNC_BRIDGEDB_METRICS=true

AUTHORITY_NAME=serge

STORE="/srv/bridges.torproject.org/from-authority-bridge-directories"
LOGDIR="/srv/bridges.torproject.org/log"

# Metrics server information.
REMOTE_USER="collector"
REMOTE_SERVER="colchicifolium.torproject.org"
DESCRIPTORS_REMOTE_PATH="/srv/collector.torproject.org/collector/in/bridge-descriptors/polyanthum/"
ASSIGNMENTS_REMOTE_PATH="/srv/collector.torproject.org/collector/in/bridge-pool-assignments/polyanthum/"
BRIDGEDB_METRICS_REMOTE_PATH="/srv/collector.torproject.org/collector/in/bridgedb-stats/"

# Sync bridge descriptors to Metris server. The source files are removed by
# rsync on success.
if test "$SYNC_DESCRIPTORS" = "true" ; then
    cd "$STORE"
    rsync \
    -a \
    --exclude from-${AUTHORITY_NAME}-latest.tar.gz \
    --remove-source-files \
    . \
    ${REMOTE_USER}@${REMOTE_SERVER}:${DESCRIPTORS_REMOTE_PATH} 2>&1 | \
    perl -lane 'print time() . ":descriptors:" . $_' >> $LOGDIR/sync-to-colchicifolium.log
fi

if test "$SYNC_ASSIGNMENTS" = "true" ; then
    cd "$LOGDIR"
    rsync \
    -a \
    assignments.log* \
    ${REMOTE_USER}@${REMOTE_SERVER}:${ASSIGNMENTS_REMOTE_PATH} 2>&1 | \
    perl -lane 'print time() . ":assignments:" . $_' >> $LOGDIR/sync-to-colchicifolium.log
fi

if test "$SYNC_BRIDGEDB_METRICS" = "true" ; then
    cd "$LOGDIR"
    rsync \
    -a \
    bridgedb-metrics.log* \
    ${REMOTE_USER}@${REMOTE_SERVER}:${BRIDGEDB_METRICS_REMOTE_PATH} 2>&1 | \
    perl -lane 'print time() . ":metrics:" . $_' >> $LOGDIR/sync-to-colchicifolium.log
fi
